
//create an instance of math.js
var math = mathjs();

//Performs calculations
function submitGpvCalc() {

	// create the variables from the given input.
	var CFs_text = document.getElementById("CF_inputs").value;
	var CFs_dump = CFs_text.split(','); //Array of Cash Flows
	var CFs = new Array();
	var P = document.getElementById("P_input").value;
	var y = document.getElementById("y_input").value;
	var m = document.getElementById("m_input").value;
	m = parseFloat(m, 10);
	var CF0 = document.getElementById("CF0_input").value;
	
	if(CF0=="")
	{
		CF0 = 0;
	}
	
	for (var i=0; i<CFs_dump.length; i++)
	{
		CFs[i] = parseFloat(CFs_dump[i], 10);
		console.log(CFs[i]);
	}
	
	var M = CFs.length;
	
	//Check for 0s in denominators
	if(y/m == (-1) || m=="0")
	{
		alert("The values you entered will cause a 0 to appear in one of the denominators. Please use different inputs.");
	}
	else
	{
		// find out if P, C, or y is blank, then solve for it.
		if((P == "") && (y != ""))
		{
			y = parseFloat(y, 10);
			P = solveForP(CFs, y, M, m, CF0);
			document.getElementById("P_input").value = math.round(P, 2);
			P = "P";
		}
		else if((P != "") && (y == ""))
		{
			P = parseFloat(P, 10);
			y = solveFory(CFs, P, M, m, CF0);
			document.getElementById("y_input").value = math.round(y, 4);
			y = "y";
		}
		else
		{
			alert("Please leave one and only one of the three spaces blank to evaluate.");
		}
		
		//Refresh the displayed equation
		var newEquation = 
			P + "= \\sum_{i=1}^{" + M + "} \\frac{CF_{i}}{(1+ \\frac{" + y + "}{" + m + "})^i}";
					
		var HTMLequation = MathJax.Hub.getAllJax("gpvEquation")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
	}
}

//Solve for P
function solveForP(CFs, y, M, m, CF0)
{
	var current = parseInt(CF0);
	
	//Echo inputs
	console.log("CFs = " + CFs);
	console.log("y = " + y);
	console.log("M = " + M);
	console.log("m = " + m);
	
	for(var i=0; i<M; i++)
	{
		current = current + CFs[i] / Math.pow((1 + (y/m)), i+1);
	}
	
	return current;
}

//Solve for y
function solveFory(CFs, P, M, m, CF0)
{
	var high = 1;
	var low = -1.1;
	//var mid = 0.123;
	var mid = 0.1
	var found = false;
	var iterations = 0;
	var tolerance = 0.0001;
	var dif;
	var y = mid;
	
	while (found == false && iterations < 100)
	{	
		var current = CF0;
		var temp = 0;
		
		dif = solveForP(CFs, y, M, m, CF0) - P;
		console.log("mid: " + mid);
		console.log("P: " + P);
		console.log("current: " + current);
		console.log("dif: " + dif);
		
		if(dif < tolerance)
		{
			high = mid;
		}
		else if(dif > tolerance) 
		{
			low = mid;
		}
		else
		{
			found = true;
		}
		mid = (high + low) / 2;
		y = mid;
		
		iterations++;
	}
	
	return y;
}

//resets all values.
function resetGpvCalc() {
	
	document.getElementById("P_input").value = "";
	document.getElementById("y_input").value = "";
	document.getElementById("m_input").value = "";
	document.getElementById("CF_inputs").value = "";
	
	var newEquation = 
	"P= \\sum_{i=1}^{M} \\frac{CF_{i}}{(1+ \\frac{y}{m})^i}";
	
	var HTMLequation = MathJax.Hub.getAllJax("gpvEquation")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
}