// create an instance of math.js
var math = mathjs();

// Performs calculations
function submitBondCalc() {

	// create the variables from the given input.
	var P = document.getElementById("P_input").value;
	var C = document.getElementById("C_input").value;
	var y = document.getElementById("y_input").value;
	var m = document.getElementById("m_input").value;
	var M_in = document.getElementById("M_input").value;
	var F = document.getElementById("F_input").value;
	
	var M = parseFloat(M_in, 10);
	
	//Check for 0s in denominators
	if(y/m == (-1) || m=="0" || y=="0")
	{
		alert("The values you entered will cause a 0 to appear in one of the"
		+" denominators. Please use different inputs.");
	}
	else
	{
		// find out if P, C, or y is blank, then solve for it.
		if((P == "") && (C != "") && (y != ""))
		{
			P = solveForP(C, y, m, M, F);
			document.getElementById("P_input").value = math.round(P, 2);
			P = "P";
		}
		else if((P != "") && (C == "") && (y != ""))
		{
			C = solveForC(P, y, m, M, F);
			document.getElementById("C_input").value = math.round(C, 2);
			C = "C";
		} 
		else if((P != "") && (C != "") && (y == ""))
		{
			y = solveFory(P, C, m, M, F);
			document.getElementById("y_input").value = math.round(y, 4);
			y = "y";
		}
		else
		{
			alert("Please leave one and only one of the three spaces blank to evaluate.");
		}
		
		//Refresh the displayed equation(s)
		var newEquation = 
			P + "=" + C + " \\cdot ( \\frac{1}{(\\frac{" + y + "}{" + m + "})} - \\frac{1}{(\\frac{" 
			+ y + "}{" + m + "})(1+ \\frac{" + y + "}{" + m + "})^{" + M + "}})";
					
		var HTMLequation = MathJax.Hub.getAllJax("BondEquation1")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
		
		newEquation =
			"+\\frac{" + F + "}{(1+ \\frac{" + y + "}{" + m + "})^{" + M + "}}";
			
		HTMLequation = MathJax.Hub.getAllJax("BondEquation2")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
	}
}

//Solve for P
function solveForP(C, y, m, M, F)
{		
	//Apologies for the mess, javascript math is apparently messed up.
	return math.eval("(" + C + ") * ( ( 1/(" + y + "/" + m + ") - 1/( (" 
	+ y + "/" + m + ") * (1 + (" + y + "/" + m + "))^" + M + " ) ) ) + ( " 
	+ F + " / ( 1 + (" + y + "/" + m + ") )^" + M+ " )");
}

//Solve for C
function solveForC(P, y, m, M, F)
{
	return math.eval("( " + P + " - ( " + F + " / ( 1 + (" + y + "/" 
	+ m + ") )^" + M + " ) ) / ( ( 1/(" + y + "/" + m + ") - 1/( (" 
	+ y + "/" + m + ") * (1 + (" + y + "/" + m + "))^" + M + " ) ) )");
}

//Problem is somewhere in here.

//Solve for y
function solveFory(P, C, m, M, F)
{
	//Bisection method
	var tolerance = 0.0001;
	var min = -100;
	var max = 101;
	var done = false;
	var minOut;
	var maxOut;
	var y;
	var midOut;
	var iterations = 0;
	
	y = min;
	minOut = solveForC(P, y, m, M, F) - C;
	
	y = max;
	maxOut = solveForC(P, y, m, M, F) - C;
		
	while(done == false && iterations < 10000) 
	{
		if(minOut < 0 && maxOut > 0)
		{
			y = (min + max) / 2;
			midOut = solveForC(P, y, m, M, F) - C;
			console.log(midOut);
			
			if (Math.abs(midOut) < tolerance)
				done = true;
			
			if (midOut > 0)
			{
				max = y;
			}
			else
			{
				min = y;
			}
			iterations++;
		}
		else
		{
			//Find out why we aren't getting the range correct.
			if(minOut < 0 && maxOut < 0)
			{
				maxOut += 1000;
			}
			else if (minOut > 0 && maxOut > 0)
			{
				minOut -= 1000;
			}
			else
			{
				alert("Rangefinding failed");
			}
		}
	}
	
	if(iterations >= 10000)
	{
		alert("bad calc");
	}	
	
	return y;
}

// resets all values.
function resetBondCalc() {
	
	document.getElementById("P_input").value = "";
	document.getElementById("C_input").value = "";
	document.getElementById("y_input").value = "";
	document.getElementById("m_input").value = "";
	document.getElementById("M_input").value = "";
	document.getElementById("F_input").value = "";
	
	var newEquation = 
	"P=C \\cdot ( \\frac{1}{(\\frac{y}{m})} - \\frac{1}{(\\frac{y}{m})(1+ \\frac{y}{m})^M})";
	
	var HTMLequation = MathJax.Hub.getAllJax("BondEquation1")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
		
	newEquation = 
	"+\\frac{F}{(1+ \\frac{y}{m})^M}";
	
	HTMLequation = MathJax.Hub.getAllJax("BondEquation2")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
}