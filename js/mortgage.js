// create an instance of math.js
var math = mathjs();

// Performs calculations
function submitMortgageCalc() {

	// create the variables from the given input.
	var P = document.getElementById("P_input").value;
	var C = document.getElementById("C_input").value;
	var y = document.getElementById("y_input").value;
	var m = document.getElementById("m_input").value;
	var M = document.getElementById("M_input").value;
	
	//Check for 0s in denominators
	if(y/m == (-1) || m=="0" || y=="0")
	{
		alert("The values you entered will cause a 0 to appear in one of the"
		+" denominators. Please use different inputs.");
	}
	else
	{
		// find out if P, C, or y is blank, then solve for it.
		if((P == "") && (C != "") && (y != ""))
		{
			P = solveForP(C, y, m, M);
			document.getElementById("P_input").value = math.round(P, 2);
			P = "P";
		}
		else if((P != "") && (C == "") && (y != ""))
		{
			C = solveForC(P, y, m, M);
			document.getElementById("C_input").value = math.round(C, 2);
			C = "C";
		} 
		else if((P != "") && (C != "") && (y == ""))
		{
			y = solveFory(P, C, m, M);
			document.getElementById("y_input").value = math.round(y, 4);
			y = "y";
		}
		else
		{
			alert("Please leave one and only one of the three spaces blank to evaluate.");
		}
		
		//Refresh the displayed equation
		var newEquation = 
			C + " = \\frac{" + P + " \\cdot \\frac{" + y + "}{" 
			+ m + "} \\cdot (1+ \\frac{" + y + "}{" + m + "})^{" 
			+ M + "}}{(1+ \\frac{" + y + "}{" + m + "})^{" + M + "}-1}";
					
		var HTMLequation = MathJax.Hub.getAllJax("MortgageEquation")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
	}
}

//Solve for P
function solveForP(C, y, m, M)
{	
	return ( C * ( Math.pow( (1 + (y/m)), M ) - 1)) / ( (y/m) * Math.pow((1 + (y/m)), M));
}

//Solve for C
function solveForC(P, y, m, M)
{
	return (P * (y/m) * Math.pow( (1+(y/m) ), M)) / ( Math.pow((1 + (y/m)), M) - 1 );
}

//Solve for y
function solveFory(P, C, m, M)
{
	//Bisection method
	var tolerance = 0.0001;
	var min = -100;
	var max = 101;
	var done = false;
	var minOut;
	var maxOut;
	var y;
	var midOut;
	var iterations = 0;
	
	y = min;
	minOut = solveForC(P, y, m, M) - C;
	
	y = max;
	maxOut = solveForC(P, y, m, M) - C;
		
	if(minOut < 0 && maxOut > 0)
	{
		while(done == false && iterations < 100) 
		{
			y = (min + max) / 2;
			midOut = solveForC(P, y, m, M) - C;
			
			if (Math.abs(midOut) < tolerance)
				done = true;
			
			if (midOut > 0)
			{
				max = y;
			}
			else
			{
				min = y;
			}
			iterations++;
		}
	}
	else
	{
		alert("Yield is out of calculation range (it would take a very long time to calculate.)")
	}
	return y;
}

// resets all values.
function resetMortgageCalc() {
	
	document.getElementById("P_input").value = "";
	document.getElementById("C_input").value = "";
	document.getElementById("y_input").value = "";
	document.getElementById("m_input").value = "";
	document.getElementById("M_input").value = "";
	
	var newEquation = 
	"C = \\frac{P \\cdot \\frac{y}{m} \\cdot (1+ \\frac{y}{m})^M}{(1+ \\frac{y}{m})^M-1}"
	
	var HTMLequation = MathJax.Hub.getAllJax("MortgageEquation")[0];
		MathJax.Hub.Queue(["Text",HTMLequation,newEquation]);
}